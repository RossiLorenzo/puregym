import requests
from bs4 import BeautifulSoup
from datetime import datetime, timedelta
import json
import os

# Get to the login page and set-up the cookies
s = requests.session()
login_url = "https://www.puregym.com/Login/"
login_response = s.request("GET", login_url)
cookies = s.cookies.get_dict()
cookie_string = "; ".join([str(x) + "=" + str(y) for x, y in cookies.items()])

# Find the request verification token of the form
soup = BeautifulSoup(login_response.text, "html.parser")
requestVerificationToken = soup.find("input", {"name": "__RequestVerificationToken"})['value']

# Do the login
headers = {
    '__requestverificationtoken': requestVerificationToken,
    'content-type': 'application/json',
    'cookie': cookie_string
}
data = '{"associateAccount":"false","email":"lorereds@gmail.com","pin":"87158830"}'
login_response = s.request("POST", "https://www.puregym.com/api/members/login/", headers=headers, data=data)

# Load the members page
members_response = s.request("GET", "https://www.puregym.com/members/")
soup = BeautifulSoup(members_response.text, "html.parser")
how_busy = soup.find("span", {"class": "heading heading--level3 secondary-color margin-bottom"}).get_text().split(' ', 1)[0]

# Write the updated data
today = datetime.now() + timedelta(hours=1)
filename = "/home/pi/raspberrypizero/PureGym/public/Data/" + today.strftime("%Y-%m-%d") + ".json"
if not os.path.exists(filename):
    with open(filename, 'w+'): pass
    data = {}
else:
    with open(filename, 'r') as json_file:
        data = json.load(json_file)

data[today.strftime("%s")] = how_busy
with open(filename, 'w') as json_file:
	json.dump(data, json_file)



print(how_busy)
